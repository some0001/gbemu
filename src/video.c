#include "video.h"
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>

#define WIDTH 160
#define HEIGHT 144

static SDL_Window* window = NULL;
static SDL_Surface* screen = NULL;
static SDL_Renderer* renderer = NULL;
static SDL_Texture* texture = NULL;

uint8_t framesRendered = 0;

void VIDEOInit()
{
    const int scale = 2;
    window = SDL_CreateWindow("GBEmu",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        WIDTH * scale, HEIGHT * scale, 0);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);
    SDL_RenderSetLogicalSize(renderer, 160, 144);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    texture = SDL_CreateTexture(renderer,
        SDL_PIXELFORMAT_RGB24,
        SDL_TEXTUREACCESS_STREAMING,
        WIDTH, HEIGHT);
}

void renderFramebuffer(uint8_t fb[144][160][3])
{
    framesRendered++;
    SDL_UpdateTexture(texture, NULL, fb, 160 * sizeof(uint8_t) * 3);

    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
}
