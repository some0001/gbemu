#include "memory.h"
#include "cpu.h"
#include "gpu.h"
#include "interrupts.h"

#define colorPalette readByte(0xFF47)
#define spritePalette1 readByte(0xFF48)
#define spritePalette2 readByte(0xFF49)

#define ScrollY readByte(0xFF42)
#define ScrollX readByte(0xFF43)
#define WindowY readByte(0xFF4A)
#define WindowX readByte(0xFF4B)

struct GPUInstance GPU;
enum GPUMode GPUMode;
uint8_t fb[144][160][3];

void GPUInit() {
	GPU.control = 0x91;
	GPU.scrollX = 0;
	GPU.scrollY = 0;
	GPU.scanline = 0;
	GPU.cycles = 0;
	GPUMode = GPU_MODE_HBLANK;
}

void GPUStep(uint32_t cycles) {
	static uint32_t lastTicks = 0;

	GPU.cycles += cycles - lastTicks;

	lastTicks = cycles;

	switch (GPUMode) {
	case GPU_MODE_HBLANK:
		if (GPU.cycles >= 204) {
			GPU.scanline++;

			if (GPU.scanline == 143) {
				renderFramebuffer(fb);

				if (IR.enable & IR_VBLANK) 
					IR.fl |= IR_VBLANK;
				
				GPUMode = GPU_MODE_VBLANK;
			}
			else GPUMode = GPU_MODE_OAM;

			GPU.cycles -= 204;
		}
		break;

	case GPU_MODE_VBLANK:
		if (GPU.cycles >= 456) {
			GPU.scanline++;

			if (GPU.scanline > 153) {
				GPU.scanline = 0;
				GPUMode = GPU_MODE_OAM;
			}

			GPU.cycles -= 456;
		}
		break;

	case GPU_MODE_OAM:
		if (GPU.cycles >= 80) {
			GPUMode = GPU_MODE_VRAM;

			GPU.cycles -= 80;
		}
		break;

	case GPU_MODE_VRAM:
		if (GPU.cycles >= 172) {
			GPUMode = GPU_MODE_HBLANK;

			if (GPU.control & CTRL_DISPLAYENABLE) {
				if (GPU.control & CTRL_BGENABLE) 
					drawBG();
				if (GPU.control & CTRL_SPRITEENABLE)
					drawSprites();
			}

			GPU.cycles -= 172;
		}
		break;
	}
}

uint8_t getBit(uint8_t inData, size_t inBitPosition) {
	uint8_t lMsk = 1 << inBitPosition;
	return (inData & lMsk) ? 1 : 0;
}

COLOUR getColour(uint8_t colourNum, uint16_t address) {
	COLOUR res = WHITE;
	uint8_t palette = readByte(address);
	int hi = 0;;
	int lo = 0;

	switch (colourNum)
	{
		case 0: hi = 1; lo = 0; break;
		case 1: hi = 3; lo = 2; break;
		case 2: hi = 5; lo = 4; break;
		case 3: hi = 7; lo = 6; break;
	}

	int colour = 0;
	colour = getBit(palette, hi) << 1;
	colour |= getBit(palette, lo);

	switch (colour)
	{
		case 0: res = WHITE; break;
		case 1: res = LIGHT_GRAY; break;
		case 2: res = DARK_GRAY; break;
		case 3: res = BLACK; break;
	}

	return res;
}


void drawBG() {
	uint16_t tileData = 0;
	uint16_t tileMap = 0;
	char unsig = 1;

	if (GPU.control & CTRL_TILESET)
		tileData = 0x8000;
	else
		tileData = 0x8800;

	if (GPU.control & CTRL_TILEMAP)
		tileMap = 0x9C00;
	else
		tileMap = 0x9800;

	uint8_t yPos = 0;

	yPos = GPU.scrollY + GPU.scanline;

	uint16_t tileRow = (((uint8_t)(yPos / 8)) * 32);

	for (int pixel = 0; pixel < 160; pixel++)
	{
		uint8_t xPos = pixel + GPU.scrollX;

		uint16_t tileCol = (xPos / 8);
		uint16_t tileNum;

		if (unsig)
			tileNum = (uint8_t)readByte(tileMap + tileRow + tileCol);
		else
			tileNum = (uint16_t)readByte(tileMap + tileRow + tileCol);

		uint16_t tileLocation = tileData;

		if (unsig)
			tileLocation += (tileNum * 16);
		else
			tileLocation += ((tileNum + 128) * 16);

		uint8_t line = yPos % 8;
		line *= 2;
		uint8_t data1 = readByte(tileLocation + line);
		uint8_t data2 = readByte(tileLocation + line + 1);

		int colourBit = xPos % 8;
		colourBit -= 7;
		colourBit *= -1;

		int colourNum = getBit(data2, colourBit);
		colourNum <<= 1;
		colourNum |= getBit(data1, colourBit);

		COLOUR col = getColour(colourNum, 0xFF47);
		uint8_t red = 0;
		uint8_t green = 0;
		uint8_t blue = 0;

		switch (col)
		{
			case WHITE:	red = 255; green = 255; blue = 255; break;
			case LIGHT_GRAY:red = 0xCC; green = 0xCC; blue = 0xCC; break;
			case DARK_GRAY:	red = 0x77; green = 0x77; blue = 0x77; break;
		}

		fb[GPU.scanline][pixel][0] = red;
		fb[GPU.scanline][pixel][1] = green;
		fb[GPU.scanline][pixel][2] = blue;
	}
}

void drawSprites() {
	for (int sprite = 0; sprite < 40; sprite++)
	{
		uint8_t index = sprite * 4;
		uint8_t yPos = readByte(0xFE00 + index) - 16;
		uint8_t xPos = readByte(0xFE00 + index + 1) - 8;
		uint8_t tileLocation = readByte(0xFE00 + index + 2);
		uint8_t attributes = readByte(0xFE00 + index + 3);

		uint8_t yFlip = (1 << 6) & attributes;
		uint8_t xFlip = (1 << 5) & attributes;


		int ysize = 8;
		if (GPU.control & CTRL_SPRITEVDOUBLE)
			ysize = 16;

		if ((GPU.scanline >= yPos) && (GPU.scanline < (yPos + ysize)))
		{
			int line = GPU.scanline - yPos;

			if (yFlip)
			{
				line -= ysize;
				line *= -1;
			}

			line *= 2;
			uint16_t dataAddress = (0x8000 + (tileLocation * 16)) + line;
			uint8_t data1 = readByte(dataAddress);
			uint8_t data2 = readByte(dataAddress + 1);
			for (int tilePixel = 7; tilePixel >= 0; tilePixel--)
			{
				int colourbit = tilePixel;
				if (xFlip)
				{
					colourbit -= 7;
					colourbit *= -1;
				}

				int colourNum = getBit(data2, colourbit);
				colourNum <<= 1;
				colourNum |= getBit(data1, colourbit);

				uint16_t colourAddress = ((1 << 4) & attributes) ? 0xFF49 : 0xFF48;
				COLOUR col = getColour(colourNum, colourAddress);

				if (col == WHITE)
					continue;

				uint8_t red = 0;
				uint8_t green = 0;
				uint8_t blue = 0;

				switch (col)
				{
					case WHITE: red = 255; green = 255; blue = 255; break;
					case LIGHT_GRAY:red = 0xCC; green = 0xCC; blue = 0xCC; break;
					case DARK_GRAY:red = 0x77; green = 0x77; blue = 0x77; break;
				}

				int xPix = 0 - tilePixel;
				xPix += 7;

				int pixel = xPos + xPix;

				if ((GPU.scanline < 0) || (GPU.scanline > 143) || (pixel < 0) || (pixel > 159))
				{
					continue;
				}

				fb[GPU.scanline][pixel][0] = red;
				fb[GPU.scanline][pixel][1] = green;
				fb[GPU.scanline][pixel][2] = blue;
			}
		}
	}
}
