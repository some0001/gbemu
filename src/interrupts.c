#include "interrupts.h"
#include "cpu.h"
#include "video.h"
#include "gpu.h"
#include "memory.h"

struct Interrupt IR;

void IRInit() {
	IR.masterEnable = 1;
	IR.enable = 0;
	IR.fl = 0;
}

void IRStep(uint32_t cycles) {
	
	if (IR.masterEnable && IR.enable && IR.fl)
	{
		unsigned char IRTrigger = IR.enable & IR.fl;

		if (IRTrigger & IR_VBLANK) {
			IR_RESET_VBLANK;
			vblank();
		}

		if (IRTrigger & IR_LCD_STAT) {
			IR_RESET_LCD_STAT;
			lcdStat();
		}

		if (IRTrigger & IR_TIMER) {
			IR_RESET_TIMER;
			timer(cycles);
		}

		if (IRTrigger & IR_SERIAL) {
			IR_RESET_SERIAL;
			serial();
		}

		if (IRTrigger & IR_JOYPAD) {
			IR_RESET_JOYPAD;
			joypad();
		}
	}
}

void vblank(void) {
	renderFramebuffer(fb);
	IR.masterEnable = 0;
	writeToStack(CPU.PC);
	CPU.PC = 0x40;

	CPU.cycles += 12;
	CPU.cycles *= 4;
}

void lcdStat(void) {
	IR.masterEnable = 0;
	writeToStack(CPU.PC);
	CPU.PC = 0x48;

	CPU.cycles += 12;
	CPU.cycles *= 4;
}


int divClocksum = 0;	
int timerClocksum = 0;

void timer(uint32_t cycles) {
	divClocksum += cycles;

	if (divClocksum >= 256) {
		divClocksum -= 256;
		writeByte(0xFF05, readByte(0xFF05) + 1);
	}

	if ((readByte(0xff07) >> 2) & 0x1) {
		timerClocksum += cycles * 4;

		int freq = 4096;                    //  Hz
		if ((readByte(0xff07) & 3) == 1)     //  mask last 2 bits
			freq = 262144;
		else if ((readByte(0xff07) & 3) == 2)    //  mask last 2 bits
			freq = 65536;
		else if ((readByte(0xff07) & 3) == 3)    //  mask last 2 bits
			freq = 16384;

		//  increment the timer according to the frequency (synched to the processed opcodes)
		while (timerClocksum >= (4194304 / freq)) {
			//  increase TIMA
			writeByte(0xFF05, readByte(0xFF05) + 1);
			//  check TIMA for overflow
			if (readByte(0xff05) == 0x00) {
				//  set timer interrupt request
				writeByte(0xff0f, readByte(0xff0f) | 4);
				//  reset timer to timer modulo
				writeByte(0xff05, readByte(0xff06));
			}
			timerClocksum -= (4194304 / freq);
		}
	}
	IR.masterEnable = 0;
	writeToStack(CPU.PC);
	CPU.PC = 0x50;

	CPU.cycles += 1;
}

void serial(void) {
	IR.masterEnable = 0;
	writeToStack(CPU.PC);
	CPU.PC = 0x58;

	CPU.cycles += 12;
}

void joypad(void) {
	IR.masterEnable = 0;
	writeToStack(CPU.PC);
	CPU.PC = 0x60;

	CPU.cycles += 12;
}



