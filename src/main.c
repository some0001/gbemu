#include "memory.h"
#include "video.h"
#include "gpu.h"
#include "loader.h"
#include "cpu.h"
#include "interrupts.h"
#include "input.h"
#include <time.h>
#include <Windows.h>
#include <stdio.h>
#include <SDL.h>


int main(int argc, char** argv) {
	uint32_t cyclesTotal = 0;

	loadROM("ROMS/acid2.gb");

	MEMInit(); CPUInit(); GPUInit(); 
	VIDEOInit(); IRInit(); InputInit();

	static clock_t clk;
	static clock_t end;

	clk = clock();

	while (1) {

		inputHandle();
		CPUStep();
		cyclesTotal += CPU.cycles;
		GPUStep(cyclesTotal*4);
		IRStep(cyclesTotal);

		if ((clock() - clk)/CLOCKS_PER_SEC >= 1) {
			printf("\rFPS: %d", framesRendered);
			framesRendered = 0;
			clk = clock();
		}
	}

	return 0;
}