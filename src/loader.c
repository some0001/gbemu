#include <stdio.h>
#include <string.h>
#include "loader.h"
#include "memory.h"

char loadROM(char* filename) {

	FILE* f;
	size_t length;

	fopen_s(&f, filename, "rb");
	if (!f) return 0;

	fseek(f, 0, SEEK_END);
	length = ftell(f);

	rewind(f);
	fread(ROM, length, 1, f);

	fclose(f);

	return 1;
}
