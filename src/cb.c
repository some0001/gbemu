#include <stdint.h>
#include "CPU.h"
#include "memory.h"


static uint8_t RLC(uint8_t value) {
    int8_t carry = (value & 0x80) >> 7;

    if (value & 0x80) SET_FLAG_C;
    else RESET_FLAG_C;

    value <<= 1;
    value += carry;

    if (value) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_N;
    RESET_FLAG_H;

    return value;
}

static uint8_t RRC(uint8_t value) {
    int8_t carry = value & 0x01;

    value >>= 1;

    if (carry) {
        SET_FLAG_C;
        value |= 0x80;
    }
    else RESET_FLAG_C;

    if (value) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_N;
    RESET_FLAG_H;

    return value;
}

static uint8_t RL(uint8_t value) {
    int8_t carry = FLAG_C;

    if (value & 0x80) SET_FLAG_C;
    else RESET_FLAG_C;

    value <<= 1;
    value += carry;

    if (value) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_N;
    RESET_FLAG_H;

    return value;
}

static uint8_t RR(uint8_t value) {
    uint8_t prevFlagC = FLAG_C;

    if (value & 0x01) SET_FLAG_C;
    else RESET_FLAG_C;

    value >>= 1;

    if (prevFlagC) value |= 0x80;

    if (value) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_N;
    RESET_FLAG_H;

    return value;
}

static uint8_t SLA(uint8_t value) {
    if (value & 0x80) SET_FLAG_C;
    else RESET_FLAG_C;

    value <<= 1;

    if (value) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_N;
    RESET_FLAG_H;

    return value;
}

static uint8_t SRA(uint8_t value) {
    if (value & 0x01) SET_FLAG_C;
    else RESET_FLAG_C;

    value = (value & 0x80) | (value >> 1);

    if (value) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_N;
    RESET_FLAG_H;

    return value;
}

static uint8_t SWAP(uint8_t value) {
    value = ((value & 0xf) << 4) | ((value & 0xf0) >> 4);

    if (value) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_C;
    RESET_FLAG_N;
    RESET_FLAG_H;

    return value;
}

static uint8_t SRL(uint8_t value) {
    if (value & 0x01) SET_FLAG_C;
    else RESET_FLAG_C;

    value >>= 1;

    if (value) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_N;
    RESET_FLAG_H;

    return value;
}

static void BIT(uint8_t bit, uint8_t value) {
    if (value & bit) RESET_FLAG_Z;
    else SET_FLAG_Z;

    RESET_FLAG_N;
    SET_FLAG_H;
}


void cbOperation(uint8_t inst) {

    switch (inst) {
    case 0x00:    // RLC B
        REG_B = RLC(REG_B);
        break;
    case 0x01:    // RLC C
        REG_C = RLC(REG_C);
        break;
    case 0x02:    // RLC D
        REG_D = RLC(REG_D);
        break;
    case 0x03:    // RLC E
        REG_E = RLC(REG_E);
        break;
    case 0x04:    // RLC H
        REG_H = RLC(REG_H);
        break;
    case 0x05:    // RLC L
        REG_L = RLC(REG_L);
        break;
    case 0x06:    // RLC (HL)
        writeByte(REG_HL, RLC(readByte(REG_HL)));
        CPU.cycles += 8;
        break;
    case 0x07:    // RLC A
        REG_A = RLC(REG_A);
        break;
    case 0x08:    // RRC B
        REG_B = RRC(REG_B);
        break;
    case 0x09:    // RRC C
        REG_C = RRC(REG_C);
        break;
    case 0x0A:    // RRC D
        REG_D = RRC(REG_D);
        break;
    case 0x0B:    // RRC E
        REG_E = RRC(REG_E);
        break;
    case 0x0C:    // RRC H
        REG_H = RRC(REG_H);
        break;
    case 0x0D:    // RRC L
        REG_L = RRC(REG_L);
        break;
    case 0x0E:    // RRC (HL)
        writeByte(REG_HL, RRC(readByte(REG_HL)));
        CPU.cycles += 8;
        break;
    case 0x0F:    // RRC A
        REG_A = RRC(REG_A);
        break;
    case 0x10:    // RL B
        REG_B = RL(REG_B);
        break;
    case 0x11:    // RL C
        REG_C = RL(REG_C);
        break;
    case 0x12:    // RL D
        REG_D = RL(REG_D);
        break;
    case 0x13:    // RL E
        REG_E = RL(REG_E);
        break;
    case 0x14:    // RL H
        REG_H = RL(REG_H);
        break;
    case 0x15:    // RL L
        REG_L = RL(REG_L);
        break;
    case 0x16:    // RL (HL)
        writeByte(REG_HL, RL(readByte(REG_HL)));
        CPU.cycles += 8;
        break;
    case 0x17:    // RL A
        REG_A = RL(REG_A);
        break;
    case 0x18:    // RR B
        REG_B = RR(REG_B);
        break;
    case 0x19:    // RR C
        REG_C = RR(REG_C);
        break;
    case 0x1A:    // RR D
        REG_D = RR(REG_D);
        break;
    case 0x1B:    // RR E
        REG_E = RR(REG_E);
        break;
    case 0x1C:    // RR H
        REG_H = RR(REG_H);
        break;
    case 0x1D:    // RR L
        REG_L = RR(REG_L);
        break;
    case 0x1E:    // RR (HL)
        writeByte(REG_HL, RR(readByte(REG_HL)));
        CPU.cycles += 8;
        break;
    case 0x1F:    // RR A
        REG_A = RR(REG_A);
        break;
    case 0x20:    // SLA B
        REG_B = SLA(REG_B);
        break;
    case 0x21:    // SLA C
        REG_C = SLA(REG_C);
        break;
    case 0x22:    // SLA D
        REG_D = SLA(REG_D);
        break;
    case 0x23:    // SLA E
        REG_E = SLA(REG_E);
        break;
    case 0x24:    // SLA H
        REG_H = SLA(REG_H);
        break;
    case 0x25:    // SLA L
        REG_L = SLA(REG_L);
        break;
    case 0x26:    // SLA HL
        writeByte(REG_HL, SLA(readByte(REG_HL)));
        CPU.cycles += 8;
        break;
    case 0x27:    // SLA A
        REG_A = SLA(REG_A);
        break;
    case 0x28:    // SRA B
        REG_B = SRA(REG_B);
        break;
    case 0x29:    // SRA C
        REG_C = SRA(REG_C);
        break;
    case 0x2A:    // SRA D
        REG_D = SRA(REG_D);
        break;
    case 0x2B:    // SRA E
        REG_E = SRA(REG_E);
        break;
    case 0x2C:    // SRA H
        REG_H = SRA(REG_H);
        break;
    case 0x2D:    // SRA L
        REG_L = SRA(REG_L);
        break;
    case 0x2E:    // SRA (HL)
        writeByte(REG_HL, SRA(readByte(REG_HL)));
        CPU.cycles += 8;
        break;
    case 0x2F:    // SRA A
        REG_A = SRA(REG_A);
        break;
    case 0x30:    // SWAP B
        REG_B = SWAP(REG_B);
        break;
    case 0x31:    // SWAP C
        REG_C = SWAP(REG_C);
        break;
    case 0x32:    // SWAP D
        REG_D = SWAP(REG_D);
        break;
    case 0x33:    // SWAP E
        REG_E = SWAP(REG_E);
        break;
    case 0x34:    // SWAP H
        REG_H = SWAP(REG_H);
        break;
    case 0x35:    // SWAP L
        REG_L = SWAP(REG_L);
        break;
    case 0x36:    // SWAP HL
        writeByte(REG_HL, SWAP(readByte(REG_HL)));
        CPU.cycles += 8;
        break;
    case 0x37:    // SWAP A
        REG_A = SWAP(REG_A);
        break;
    case 0x38:    // SRL B
        REG_B = SRL(REG_B);
        break;
    case 0x39:    // SRL C
        REG_C = SRL(REG_C);
        break;
    case 0x3A:    // SRL D
        REG_D = SRL(REG_D);
        break;
    case 0x3B:    // SRL E
        REG_E = SRL(REG_E);
        break;
    case 0x3C:    // SRL H
        REG_H = SRL(REG_H);
        break;
    case 0x3D:    // SRL L
        REG_L = SRL(REG_L);
        break;
    case 0x3E:    // SRL (HL)
        writeByte(REG_HL, SRL(readByte(REG_HL)));
        CPU.cycles += 8;
        break;
    case 0x3F:    // SRL A
        REG_A = SRL(REG_A);
        break;
    case 0x40:    // BIT B 0
        BIT(1 << 0, REG_B);
        break;
    case 0x41:    // BIT C 0
        BIT(1 << 0, REG_C);
        break;
    case 0x42:    // BIT D 0
        BIT(1 << 0, REG_D);
        break;
    case 0x43:    // BIT E 0
        BIT(1 << 0, REG_E);
        break;
    case 0x44:    // BIT H 0
        BIT(1 << 0, REG_H);
        break;
    case 0x45:    // BIT L 0
        BIT(1 << 0, REG_L);
        break;
    case 0x46:    // BIT (HL) 0
        BIT(1 << 0, readByte(REG_HL));
        CPU.cycles += 8;
        break;
    case 0x47:    // BIT A 0
        BIT(1 << 0, REG_A);
        break;
    case 0x48:    // BIT B 1
        BIT(1 << 1, REG_B);
        break;
    case 0x49:    // BIT C 1
        BIT(1 << 1, REG_C);
        break;
    case 0x4A:    // BIT D 1
        BIT(1 << 1, REG_D);
        break;
    case 0x4B:    // BIT E 1
        BIT(1 << 1, REG_E);
        break;
    case 0x4C:    // BIT H 1
        BIT(1 << 1, REG_H);
        break;
    case 0x4D:    // BIT L 1
        BIT(1 << 1, REG_L);
        break;
    case 0x4E:    // BIT (HL) 1
        BIT(1 << 1, readByte(REG_HL));
        CPU.cycles += 8;
        break;
    case 0x4F:    // BIT A 1
        BIT(1 << 1, REG_A);
        break;
    case 0x50:    // BIT B 2
        BIT(1 << 2, REG_B);
        break;
    case 0x51:    // BIT C 2
        BIT(1 << 2, REG_C);
        break;
    case 0x52:    // BIT D 2
        BIT(1 << 2, REG_D);
        break;
    case 0x53:    // BIT E 2
        BIT(1 << 2, REG_E);
        break;
    case 0x54:    // BIT H 2
        BIT(1 << 2, REG_H);
        break;
    case 0x55:    // BIT L 2
        BIT(1 << 2, REG_L);
        break;
    case 0x56:    // BIT (HL) 2
        BIT(1 << 2, readByte(REG_HL));
        CPU.cycles += 8;
        break;
    case 0x57:    // BIT A 2
        BIT(1 << 2, REG_A);
        break;
    case 0x58:    // BIT B 3
        BIT(1 << 3, REG_B);
        break;
    case 0x59:    // BIT C 3
        BIT(1 << 3, REG_C);
        break;
    case 0x5A:    // BIT D 3
        BIT(1 << 3, REG_D);
        break;
    case 0x5B:    // BIT E 3
        BIT(1 << 3, REG_E);
        break;
    case 0x5C:    // BIT H 3
        BIT(1 << 3, REG_H);
        break;
    case 0x5D:    // BIT L 3
        BIT(1 << 3, REG_L);
        break;
    case 0x5E:    // BIT (HL) 3
        BIT(1 << 3, readByte(REG_HL));
        CPU.cycles += 8;
        break;
    case 0x5F:    // BIT A 3
        BIT(1 << 3, REG_A);
        break;
    case 0x60:    // BIT B 4
        BIT(1 << 4, REG_B);
        break;
    case 0x61:    // BIT C 4
        BIT(1 << 4, REG_C);
        break;
    case 0x62:    // BIT D 4
        BIT(1 << 4, REG_D);
        break;
    case 0x63:    // BIT E 4
        BIT(1 << 4, REG_E);
        break;
    case 0x64:    // BIT H 4
        BIT(1 << 4, REG_H);
        break;
    case 0x65:    // BIT L 4
        BIT(1 << 4, REG_L);
        break;
    case 0x66:    // BIT (HL) 4
        BIT(1 << 4, readByte(REG_HL));
        CPU.cycles += 8;
        break;
    case 0x67:    // BIT A 4
        BIT(1 << 4, REG_A);
        break;
    case 0x68:    // BIT B 5
        BIT(1 << 5, REG_B);
        break;
    case 0x69:    // BIT C 5
        BIT(1 << 5, REG_C);
        break;
    case 0x6A:    // BIT D 5
        BIT(1 << 5, REG_D);
        break;
    case 0x6B:    // BIT E 5
        BIT(1 << 5, REG_E);
        break;
    case 0x6C:    // BIT H 5
        BIT(1 << 5, REG_H);
        break;
    case 0x6D:    // BIT L 5
        BIT(1 << 5, REG_L);
        break;
    case 0x6E:    // BIT (HL) 5
        BIT(1 << 5, readByte(REG_HL));
        CPU.cycles += 8;
        break;
    case 0x6F:    // BIT A 5
        BIT(1 << 5, REG_A);
        break;
    case 0x70:    // BIT B 6
        BIT(1 << 6, REG_B);
        break;
    case 0x71:    // BIT C 6
        BIT(1 << 6, REG_C);
        break;
    case 0x72:    // BIT D 6
        BIT(1 << 6, REG_D);
        break;
    case 0x73:    // BIT E 6
        BIT(1 << 6, REG_E);
        break;
    case 0x74:    // BIT H 6
        BIT(1 << 6, REG_H);
        break;
    case 0x75:    // BIT L 6
        BIT(1 << 6, REG_L);
        break;
    case 0x76:    // BIT (HL) 6
        BIT(1 << 6, readByte(REG_HL));
        CPU.cycles += 8;
        break;
    case 0x77:    // BIT A 6
        BIT(1 << 6, REG_A);
        break;
    case 0x78:    // BIT B 7
        BIT(1 << 7, REG_B);
        break;
    case 0x79:    // BIT C 7
        BIT(1 << 7, REG_C);
        break;
    case 0x7A:    // BIT D 7
        BIT(1 << 7, REG_D);
        break;
    case 0x7B:    // BIT E 7
        BIT(1 << 7, REG_E);
        break;
    case 0x7C:    // BIT H 7
        BIT(1 << 7, REG_H);
        break;
    case 0x7D:    // BIT L 7
        BIT(1 << 7, REG_L);
        break;
    case 0x7E:    // BIT (HL) 7
        BIT(1 << 7, readByte(REG_HL));
        CPU.cycles += 8;
        break;
    case 0x7F:    // BIT A 7
        BIT(1 << 7, REG_A);
        break;
    case 0x80:    // RES B 0
        REG_B &= ~(1 << 0);
        break;
    case 0x81:    // RES C 0
        REG_C &= ~(1 << 0);
        break;
    case 0x82:    // RES D 0
        REG_D &= ~(1 << 0);
        break;
    case 0x83:    // RES E 0
        REG_E &= ~(1 << 0);
        break;
    case 0x84:    // RES H 0
        REG_H &= ~(1 << 0);
        break;
    case 0x85:    // RES L 0
        REG_L &= ~(1 << 0);
        break;
    case 0x86:    // RES (HL) 0
        writeByte(REG_HL, (readByte(REG_HL) & 0xFE));
        CPU.cycles += 8;
        break;
    case 0x87:    // RES A 0
        REG_A &= ~(1 << 0);
        break;
    case 0x88:    // RES B 1
        REG_B &= ~(1 << 1);
        break;
    case 0x89:    // RES C 1
        REG_C &= ~(1 << 1);
        break;
    case 0x8A:    // RES D 1
        REG_D &= ~(1 << 1);
        break;
    case 0x8B:    // RES E 1
        REG_E &= ~(1 << 1);
        break;
    case 0x8C:    // RES H 1
        REG_H &= ~(1 << 1);
        break;
    case 0x8D:    // RES L 1
        REG_L &= ~(1 << 1);
        break;
    case 0x8E:    // RES (HL) 1
        writeByte(REG_HL, (readByte(REG_HL) & 0xFD));
        CPU.cycles += 8;
        break;
    case 0x8F:    // RES A 1
        REG_A &= ~(1 << 1);
        break;
    case 0x90:    // RES B 2
        REG_B &= ~(1 << 2);
        break;
    case 0x91:    // RES C 2
        REG_C &= ~(1 << 2);
        break;
    case 0x92:    // RES D 2
        REG_D &= ~(1 << 2);
        break;
    case 0x93:    // RES E 2
        REG_E &= ~(1 << 2);
        break;
    case 0x94:    // RES H 2
        REG_H &= ~(1 << 2);
        break;
    case 0x95:    // RES L 2
        REG_L &= ~(1 << 2);
        break;
    case 0x96:    // RES (HL) 2
        writeByte(REG_HL, (readByte(REG_HL) & 0xFB));
        CPU.cycles += 8;
        break;
    case 0x97:    // RES A 2
        REG_A &= ~(1 << 2);
        break;
    case 0x98:    // RES B 3
        REG_B &= ~(1 << 3);
        break;
    case 0x99:    // RES C 3
        REG_C &= ~(1 << 3);
        break;
    case 0x9A:    // RES D 3
        REG_D &= ~(1 << 3);
        break;
    case 0x9B:    // RES E 3
        REG_E &= ~(1 << 3);
        break;
    case 0x9C:    // RES H 3
        REG_H &= ~(1 << 3);
        break;
    case 0x9D:    // RES L 3
        REG_L &= ~(1 << 3);
        break;
    case 0x9E:    // RES (HL) 3
        writeByte(REG_HL, (readByte(REG_HL) & 0xF7));
        CPU.cycles += 8;
        break;
    case 0x9F:    // RES A 3
        REG_A &= ~(1 << 3);
        break;
    case 0xA0:    // RES B 4
        REG_B &= ~(1 << 4);
        break;
    case 0xA1:    // RES C 4
        REG_C &= ~(1 << 4);
        break;
    case 0xA2:    // RES D 4
        REG_D &= ~(1 << 4);
        break;
    case 0xA3:    // RES E 4
        REG_E &= ~(1 << 4);
        break;
    case 0xA4:    // RES H 4
        REG_H &= ~(1 << 4);
        break;
    case 0xA5:    // RES L 4
        REG_L &= ~(1 << 4);
        break;
    case 0xA6:    // RES (HL) 4
        writeByte(REG_HL, (readByte(REG_HL) & 0xEF));
        CPU.cycles += 8;
        break;
    case 0xA7:    // RES A 4
        REG_A &= ~(1 << 4);
        break;
    case 0xA8:    // RES B 5
        REG_B &= ~(1 << 5);
        break;
    case 0xA9:    // RES C 5
        REG_C &= ~(1 << 5);
        break;
    case 0xAA:    // RES D 5
        REG_D &= ~(1 << 5);
        break;
    case 0xAB:    // RES E 5
        REG_E &= ~(1 << 5);
        break;
    case 0xAC:    // RES H 5
        REG_H &= ~(1 << 5);
        break;
    case 0xAD:    // RES L 5
        REG_L &= ~(1 << 5);
        break;
    case 0xAE:    // RES (HL) 5
        writeByte(REG_HL, (readByte(REG_HL) & 0xDF));
        CPU.cycles += 8;
        break;
    case 0xAF:    // RES A 5
        REG_A &= ~(1 << 5);
        break;
    case 0xB0:    // RES B 6
        REG_B &= ~(1 << 6);
        break;
    case 0xB1:    // RES C 6
        REG_C &= ~(1 << 6);
        break;
    case 0xB2:    // RES D 6
        REG_D &= ~(1 << 6);
        break;
    case 0xB3:    // RES E 6
        REG_E &= ~(1 << 6);
        break;
    case 0xB4:    // RES H 6
        REG_H &= ~(1 << 6);
        break;
    case 0xB5:    // RES L 6
        REG_L &= ~(1 << 6);
        break;
    case 0xB6:    // RES (HL) 6
        writeByte(REG_HL, (readByte(REG_HL) & 0xBF));
        CPU.cycles += 8;
        break;
    case 0xB7:    // RES A 6
        REG_A &= ~(1 << 6);
        break;
    case 0xB8:    // RES B 7
        REG_B &= ~(1 << 7);
        break;
    case 0xB9:    // RES C 7
        REG_C &= ~(1 << 7);
        break;
    case 0xBA:    // RES D 7
        REG_D &= ~(1 << 7);
        break;
    case 0xBB:    // RES E 7
        REG_E &= ~(1 << 7);
        break;
    case 0xBC:    // RES H 7
        REG_H &= ~(1 << 7);
        break;
    case 0xBD:    // RES L 7
        REG_L &= ~(1 << 7);
        break;
    case 0xBE:    // RES (HL) 7
        writeByte(REG_HL, (readByte(REG_HL) & 0x7F));
        CPU.cycles += 8;
        break;
    case 0xBF:    // RES A 7
        REG_A &= ~(1 << 7);
        break;
    case 0xC0:    // SET B 0
        REG_B |= 0x01;
        break;
    case 0xC1:    // SET C 0
        REG_C |= 0x01;
        break;
    case 0xC2:    // SET D 0
        REG_D |= 0x01;
        break;
    case 0xC3:    // SET E 0
        REG_E |= 0x01;
        break;
    case 0xC4:    // SET H 0
        REG_H |= 0x01;
        break;
    case 0xC5:    // SET L 0
        REG_L |= 0x01;
        break;
    case 0xC6:    // SET (HL) 0
        writeByte(REG_HL, (readByte(REG_HL) | 0x01));
        CPU.cycles += 8;
        break;
    case 0xC7:    // SET A 0
        REG_A |= 0x01;
        break;
    case 0xC8:    // SET B 1
        REG_B |= 0x02;
        break;
    case 0xC9:    // SET C 1
        REG_C |= 0x02;
        break;
    case 0xCA:    // SET D 1
        REG_D |= 0x02;
        break;
    case 0xCB:    // SET E 1
        REG_E |= 0x02;
        break;
    case 0xCC:    // SET H 1
        REG_H |= 0x02;
        break;
    case 0xCD:    // SET L 1
        REG_L |= 0x02;
        break;
    case 0xCE:    // SET (HL) 1
        writeByte(REG_HL, (readByte(REG_HL) | 0x02));
        CPU.cycles += 8;
        break;
    case 0xCF:    // SET A 1
        REG_A |= 0x02;
        break;
    case 0xD0:    // SET B 2
        REG_B |= 0x04;
        break;
    case 0xD1:    // SET C 2
        REG_C |= 0x04;
        break;
    case 0xD2:    // SET D 2
        REG_D |= 0x04;
        break;
    case 0xD3:    // SET E 2
        REG_E |= 0x04;
        break;
    case 0xD4:    // SET H 2
        REG_H |= 0x04;
        break;
    case 0xD5:    // SET L 2
        REG_L |= 0x04;
        break;
    case 0xD6:    // SET (HL) 2
        writeByte(REG_HL, (readByte(REG_HL) | 0x04));
        CPU.cycles += 8;
        break;
    case 0xD7:    // SET A 2
        REG_A |= 0x04;
        break;
    case 0xD8:    // SET B 3
        REG_B |= 0x08;
        break;
    case 0xD9:    // SET C 3
        REG_C |= 0x08;
        break;
    case 0xDA:    // SET D 3
        REG_D |= 0x08;
        break;
    case 0xDB:    // SET E 3
        REG_E |= 0x08;
        break;
    case 0xDC:    // SET H 3
        REG_H |= 0x08;
        break;
    case 0xDD:    // SET L 3
        REG_L |= 0x08;
        break;
    case 0xDE:    // SET (HL) 3
        writeByte(REG_HL, (readByte(REG_HL) | 0x08));
        CPU.cycles += 8;
        break;
    case 0xDF:    // SET A 3
        REG_A |= 0x08;
        break;
    case 0xE0:    // SET B 4
        REG_B |= 0x10;
        break;
    case 0xE1:    // SET C 4
        REG_C |= 0x10;
        break;
    case 0xE2:    // SET D 4
        REG_D |= 0x10;
        break;
    case 0xE3:    // SET E 4
        REG_E |= 0x10;
        break;
    case 0xE4:    // SET H 4
        REG_H |= 0x10;
        break;
    case 0xE5:    // SET L 4
        REG_L |= 0x10;
        break;
    case 0xE6:    // SET (HL) 4
        writeByte(REG_HL, (readByte(REG_HL) | 0x10));
        CPU.cycles += 8;
        break;
    case 0xE7:    // SET A 4
        REG_A |= 0x10;
        break;
    case 0xE8:    // SET B 5
        REG_B |= 0x20;
        break;
    case 0xE9:    // SET C 5
        REG_C |= 0x20;
        break;
    case 0xEA:    // SET D 5
        REG_D |= 0x20;
        break;
    case 0xEB:    // SET E 5
        REG_E |= 0x20;
        break;
    case 0xEC:    // SET H 5
        REG_H |= 0x20;
        break;
    case 0xED:    // SET L 5
        REG_L |= 0x20;
        break;
    case 0xEE:    // SET (HL) 5
        writeByte(REG_HL, (readByte(REG_HL) | 0x20));
        CPU.cycles += 8;
        break;
    case 0xEF:    // SET A 5
        REG_A |= 0x20;
        break;
    case 0xF0:    // SET B 6
        REG_B |= 0x40;
        break;
    case 0xF1:    // SET C 6
        REG_C |= 0x40;
        break;
    case 0xF2:    // SET D 6
        REG_D |= 0x40;
        break;
    case 0xF3:    // SET E 6
        REG_E |= 0x40;
        break;
    case 0xF4:    // SET H 6
        REG_H |= 0x40;
        break;
    case 0xF5:    // SET L 6
        REG_L |= 0x40;
        break;
    case 0xF6:    // SET (HL) 6
        writeByte(REG_HL, (readByte(REG_HL) | 0x40));
        CPU.cycles += 8;
        break;
    case 0xF7:    // SET A 6
        REG_A |= 0x40;
        break;
    case 0xF8:    // SET B 7
        REG_B |= 0x80;
        break;
    case 0xF9:    // SET C 7
        REG_C |= 0x80;
        break;
    case 0xFA:    // SET D 7
        REG_D |= 0x80;
        break;
    case 0xFB:    // SET E 7
        REG_E |= 0x80;
        break;
    case 0xFC:    // SET H 7
        REG_H |= 0x80;
        break;
    case 0xFD:    // SET L 7
        REG_L |= 0x80;
        break;
    case 0xFE:    // SET (HL) 7
        writeByte(REG_HL, (readByte(REG_HL) | 0x80));
        CPU.cycles += 8;
        break;
    case 0xFF:    // SET A 7
        REG_A |= 0x80;
        break;
    }
}
