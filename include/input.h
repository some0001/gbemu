#pragma once
#include <SDL.h>

#define INPUT_SELECT_BUTTONS	0x20
#define INPUT_SELECT_DIRECTIONS	0x10

#define INPUT_BUTTONS_START	0x08
#define INPUT_BUTTONS_SELECT	0x04
#define INPUT_BUTTONS_B		0x02
#define INPUT_BUTTONS_A		0x01

#define INPUT_DIRECTIONS_DOWN	0x08
#define INPUT_DIRECTIONS_UP	0x04
#define INPUT_DIRECTIONS_LEFT	0x02
#define INPUT_DIRECTIONS_RIGHT	0x01


struct Joypad {
	uint8_t buttons;
	uint8_t directions;
	uint8_t select;
}extern Joypad;

void InputInit();
int keyInputEventFilter(void* null, SDL_Event* event);
void inputHandle();
