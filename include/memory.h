#pragma once

#include <stdint.h> 

//0000 - 7FFF
extern uint8_t ROM[0x8000]; //32KB
//8000 - 9FFF
extern uint8_t videoRAM[0x2000]; //8KB
//A000 - BFFF
extern uint8_t extRAM[0x2000]; //8KB
//C000 - FDFF
extern uint8_t workRAM[0x2000]; //8KB
//FE00 - FE9F
extern uint8_t OAM[0x100]; //256B
//FF00 - FF7E
extern uint8_t IO[0x100]; //256B
///FF80 - FFFE
extern uint8_t highRAM[0x80]; //128B

extern const uint8_t ioReset[0x100];

#define ROM_START 0x0000
#define ROM_END 0x7FFF
#define videoRAM_START 0x8000
#define videoRAM_END 0x9FFF
#define extRAM_START 0xA000
#define extRAM_END 0xBFFF
#define workRAM_START 0xC000
#define workRAM_END 0xFDFF
#define OAM_START 0xFE00
#define OAM_END 0xFEFF
#define IO_START 0xFF00
#define IO_END 0xFF7F
#define highRAM_START 0xFF80
#define highRAM_END 0xFFFE
#define IR_ENABLE 0xFFFF
#define IR_FL 0xFF0F


void MEMInit();
uint8_t readByte(uint16_t address);
uint16_t read2Bytes(uint16_t address);
uint16_t readFromStack();
void writeByte(uint16_t address, uint8_t value);
void write2Bytes(uint16_t address, uint16_t value);
void writeToStack(uint16_t value);

