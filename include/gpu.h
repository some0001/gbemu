#pragma once
struct GPUInstance {
	uint8_t control;
	uint8_t scrollX;
	uint8_t scrollY;
	uint8_t scanline;
	uint32_t cycles;
} extern GPU;

enum GPUMode {
	GPU_MODE_HBLANK = 0,
	GPU_MODE_VBLANK = 1,
	GPU_MODE_OAM = 2,
	GPU_MODE_VRAM = 3,
} extern GPUMode;

typedef enum COLOUR
{
	WHITE,
	LIGHT_GRAY,
	DARK_GRAY,
	BLACK
} COLOUR;

#define CTRL_BGENABLE (1 << 0)
#define CTRL_SPRITEENABLE (1 << 1)
#define CTRL_SPRITEVDOUBLE (1 << 2)
#define CTRL_TILEMAP (1 << 3)
#define CTRL_TILESET (1 << 4)
#define CTRL_WINDOWENABLE (1 << 5)
#define CTRL_WINDOWTILEMAP (1 << 6)
#define CTRL_DISPLAYENABLE (1 << 7)

#define tileArea1 0x8000
#define tileArea2  0x8800
#define tileArea2Offset 128



extern uint8_t fb[144][160][3];

void GPUInit();
void GPUStep(uint32_t cycles);
void drawBG();
void drawSprites();

